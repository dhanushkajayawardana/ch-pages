<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TransferDataToPageTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // move the existing data into the translations table
        DB::statement(
            'insert into page_translations (page_id, locale, page_title, page_slug, page_feature_media_type, page_image, page_video, page_meta_title, page_meta_description, page_visibility, page_body, page_author, page_read_time, page_date)
                select id, :locale, page_title, page_slug, page_feature_media_type, page_image, page_video, page_meta_title, page_meta_description, page_visibility, page_body, page_author, page_read_time, page_date
                from pages',
            ['locale' => config('app.locale')])
        ;
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Move the translated data back into the main table. Translations of the default language of the application are selected.
        DB::statement(
            'update pages as m
                join page_translations as t on m.id = t.page_id
                set m.page_title = t.page_title,
                    m.page_slug = t.page_slug,
                    m.page_feature_media_type = t.page_feature_media_type,
                    m.page_image = t.page_image,
                    m.page_video = t.page_video,
                    m.page_meta_title = t.page_meta_title,
                    m.page_meta_description = t.page_meta_description,
                    m.page_visibility = t.page_visibility,
                    m.page_body = t.page_body,
                    m.page_author = t.page_author,
                    m.page_read_time = t.page_read_time,
                    m.page_date = t.page_date
                where t.locale = :locale',
            ['locale' => config('app.locale')])
        ;
    }
}
