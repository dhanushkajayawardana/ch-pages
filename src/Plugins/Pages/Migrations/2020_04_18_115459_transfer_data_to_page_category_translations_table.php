<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TransferDataToPageCategoryTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // move the existing data into the translations table
        DB::statement(
            'insert into page_category_translations (page_category_id, locale, category_name, category_slug, category_description, seo_title, seo_url, seo_description)
                select id, :locale, category_name, category_slug, category_description, seo_title, seo_url, seo_description
                from page_category',
            ['locale' => config('app.locale')])
        ;
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Move the translated data back into the main table. Translations of the default language of the application are selected.
        DB::statement(
            'update page_category as m
                join page_category_translations as t on m.id = t.page_category_id
                set m.category_name = t.category_name, m.category_slug = t.category_slug, m.category_description = t.category_description, m.seo_title = t.seo_title, m.seo_url = t.seo_url, m.seo_description = t.seo_description
                where t.locale = :locale',
            ['locale' => config('app.locale')])
        ;
    }
}
