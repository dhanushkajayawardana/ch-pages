<?php

namespace Creativehandles\ChPages\Plugins\Pages\Seeders;

class PagesSeeder
{
    public function run()
    {
         $this->seedCategory();
    }

    //seed uncategorized category
    protected function seedCategory()
    {
        \DB::table('page_category')->truncate();
        \DB::table('page_category')->insert([
            'id'=>1,
            'category_name'=>'uncategorized',
            'category_slug'=>'uncategorized'
        ]);
    }
}