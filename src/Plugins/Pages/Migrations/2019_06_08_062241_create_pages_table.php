<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pages', function (Blueprint $table) {
            $table->increments('id');
            $table->string('page_title');
            $table->string('page_slug');
            $table->string('page_feature_media_type')->default('img');
            $table->text('page_image')->nullable();
            $table->text('page_video')->nullable();
            $table->string('page_meta_title')->nullable();
            $table->text('page_meta_description')->nullable();
            $table->integer('page_visibility')->nullable()->default(2);
            $table->string('page_language')->nullable()->default('en');
            $table->longText('page_body');
            $table->integer('page_author');
            $table->string('page_read_time')->nullable();
            $table->dateTime('page_date')->nullable();
            $table->string('deleted_by')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pages');
    }
}
