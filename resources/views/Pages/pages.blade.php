@extends('Admin.layout')

@section("styles")
    <link rel="stylesheet" type="text/css" href="{{ asset("vendors/css/ui/dragula.min.css") }}">
    <link rel="stylesheet" type="text/css" href="{{ asset("vendors/css/extensions/toastr.css") }}">
    <link rel="stylesheet" type="text/css" href="{{ asset("css/plugins/extensions/toastr.css") }}">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet"/>

@endsection

@section('content')
    @include('Admin.partials.breadcumbs',['header'=>__('general.Page')])

    <div class="content-body">

        <div class="row">
            <div class="col-md-12 form-card" >
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title" id="basic-layout-form-center">{{__('pages.Create a new page')}}</h4>
                    </div>
                    <div class="card-content collapse show">
                        <div class="card-body">
                            <form action="{{route('admin.createNewPage')}}" method="post" id="pageForm"
                                  enctype="multipart/form-data">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-body">
                                            {{csrf_field()}}

                                            <div class="row">
                                                <div class="col-md-6">
                                                    {{--form_locale--}}
                                                    @include('Admin.partials.multi-language-select-locale', ['setRowCol' => false, 'selectId' => 'page-locale'])
                                                </div>
                                                <div class="col-md-6">
                                                    {{--page visibility--}}
                                                    <div class="form-group">
                                                        <label>{{__('pages.Page visibility')}}</label>
                                                        <select required class="custom-select block" name="visibility">
                                                            <option disabled>{{__('pages.Please select a value')}}</option>
                                                            <option @if (old('visibility')) {{ (old('visibility') == '1') ? 'selected' : '' }} @else selected
                                                                    @endif value="1">{{__('pages.Visible to all')}}
                                                            </option>
                                                            <option @if (old('visibility')) {{ (old('visibility') == '2') ? 'selected' : '' }}  @endif value="2">
                                                                {{__('pages.Hidden for public')}}
                                                            </option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>

                                            {{--title--}}
                                            <div class="form-group">
                                                <label>{{__('pages.Page Title')}}</label>
                                                <input required type="text" class="form-control" id="page_title"
                                                       placeholder="Page Title"
                                                       name="title" value="{{old('title')}}">
                                            </div>
                                            <input type="hidden" name="source_id" id="source_id">
                                            {{--url--}}
                                            <div class="form-group">
                                                <label>{{__('pages.Page URL')}}
                                                    <a title="show preview" id="show-preview" class="hide show-url-preview"
                                                        href="{{ \Creativehandles\ChPages\ChPagesFacade::buildPreviewUrl('', $availableId ?? 1, '') }}"
                                                        target="_blank">
                                                        <i class="fa fa-external-link-square"></i>
                                                    </a>
                                                </label>
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" id="span-url">{{ \Creativehandles\ChPages\ChPagesFacade::buildPreviewUrl('', $availableId ?? 1, '') }}</span>
                                                    </div>
                                                    <input required type="text" class="form-control" id="page_slug"
                                                           placeholder="Page URL"
                                                           name="page_slug" value="{{old('page_slug')}}">
                                                </div>
                                            </div>

                                            {{--meta-title--}}
                                            <div class="form-group">
                                                <label>{{__('pages.Meta Title')}}</label>
                                                <input type="text" class="form-control"
                                                       placeholder="Meta Title"
                                                       name="meta_title" value="{{old('meta_title')}}">
                                            </div>

                                            {{--meta description--}}
                                            <div class="form-group">
                                                <label>{{__('pages.Meta description')}}</label>
                                                <textarea required type="text" class="form-control"
                                                          placeholder="Meta description"
                                                          name="meta_description"
                                                          rows="3">{{old('meta_description')}}</textarea>
                                            </div>
                                            <div class="row hide">
                                                <div class="col-md-6">
                                                    <label>{{__('pages.Page date')}}</label>
                                                    <input type="datetime" name="page_date" class="form-control"
                                                           value="{{old('page_date')}}" placeholder="Page Date">
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>{{__('pages.Read time')}}</label>
                                                        <input type="text" name="page_read_time" class="form-control"
                                                               value="{{old('read_time')}}"
                                                               placeholder="{{__('general.Estimated page read time')}}">
                                                    </div>
                                                </div>
                                            </div>

                                            {{--featured type--}}
                                            <div class="form-group">
                                                <label>{{__('pages.Page Featured Media Type')}}</label>
                                                <fieldset class="radio">
                                                    <label>
                                                        <input type="radio" checked required name="feature_type"
                                                               value="img" class="feature_type">
                                                        {{__('pages.Image')}}
                                                    </label>
                                                </fieldset>
                                                <fieldset class="radio">
                                                    <label>
                                                        <input type="radio" required name="feature_type" value="vdo"
                                                               class="feature_type">
                                                        {{__('pages.Video')}}
                                                    </label>
                                                </fieldset>
                                            </div>

                                            {{--featured image--}}
                                            <div class="form-group hide" id="feature-img-container">
                                                <label>{{__('pages.Page Featured Image')}}</label>
                                                <input id="page_image" type="file" class="form-control"
                                                       name="page_image">
                                                <img id="page_image_preview" class="hide height-150 img-thumbnail">
                                            </div>

                                            {{--featured image--}}
                                            <div class="form-group hide" id="feature-vdo-container">
                                                <label>{{__('pages.Page Featured Video')}}</label>
                                                <input id="" type="text" class="form-control"
                                                       placeholder="Place Youtube Video URL" name="page_video">
                                            </div>

                                            <div class="form-group">
                                                <div class="form-group mb-2 attachment-repeater">
                                                    <label>{{__('pages.Attachments')}}</label>
                                                    {!! Gallery::RenderMultipleImageSelector() !!}
                                                </div>
                                            </div>

                                            <div class="form-group ">
                                                <label for="categoryList">{{__('pages.Page category')}}</label>
                                                <select class="select2 form-control" id="categoryList" multiple
                                                        name="categories[]">
                                                    @foreach($categories as $category)
                                                        <option value="{{$category->id}}">
                                                            <span>{{($category->parent)? $category->parent->category_name.' - ':''}}</span> {{$category->category_name}}
                                                        </option>
                                                    @endforeach
                                                </select>

                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <a href="#" id="newCategory"><b>{{__('pages.New category?')}}</b></a>
                                                    </div>
                                                </div>
                                            </div>

                                            {{--body--}}
                                            <div class="form-group">
                                                <label>{{__('pages.Page body')}}</label>
                                                <textarea name="body" id="pageBody" cols="30" rows="10"
                                                          placeholder="body"
                                                          class="form-control summernote-body"></textarea>
                                            </div>
                                            {{--link to tags--}}
                                            <div class="form-group">
                                                <label>{{__('pages.Page Tags')}}</label>
                                                <select class="form-control" id="tagSelector"
                                                        multiple="multiple" aria-hidden="true"
                                                        name="tags[]">
                                                    @foreach($tags as $tag)
                                                        <option @if(is_array(old('tags[]')) && in_array($tag->label, old('tags[]'))) selected
                                                                @endif value="{{$tag->label}}">{{$tag->label}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-actions left">
                                            <button type="reset" class="btn btn-warning mr-1">
                                                <i class="ft-x"></i> {{__('pages.Clear')}}
                                            </button>
                                            <a href="{{ URL::previous() }}"><button type="button" href="" class="btn btn-warning mr-1">
                                                    <i class="ft-arrow-left"></i> {{__('pages.Go Back')}}
                                                </button></a>
                                            <button type="button" class="btn btn-primary" id="saveForm">
                                                <i class="fa fa-check-square-o"></i> {{__('pages.Save')}}
                                            </button>
                                            {{--<button type="submit" class="btn btn-primary" id="saveAndGoBack">
                                                <i class="fa fa-check-square-o"></i> {{__('pages.Save and Go back')}}
                                            </button>--}}
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card hide" id="category-card">
                    <div class="card-header">
                        <h4 class="card-title" id="basic-layout-form-center">{{__('pages.Create new Category')}}</h4>
                    </div>
                    <div class="card-content collapse show">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12">
                                    @include('Admin.Pages.new-category-form')
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section("scripts")
    <script src="{{ asset("vendors/js/extensions/sweetalert.min.js") }}" type="text/javascript"></script>
    <script src="{{ asset("vendors/js/extensions/dragula.min.js") }}" type="text/javascript"></script>
    <script src="{{ asset("vendors/js/extensions/toastr.min.js") }}" type="text/javascript"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>
    <script>
        $(document).ready(function () {

            var changeSelector = '.show-url-preview';
            //app url
            var feBaseUrl = "{{ \Creativehandles\ChPages\ChPagesFacade::buildFeBaseUrl() }}";
            var urlPreview = $(changeSelector).attr('href');

            //set url on page load
            var currLocale = $('#page-locale').val();
            var replacedUrl = urlPreview.replace(feBaseUrl, feBaseUrl + '/' + currLocale);
            $(changeSelector).attr('href',replacedUrl);

            var spanCurUrl = $('#span-url').html();
            $('#span-url').html(spanCurUrl.replace(feBaseUrl, feBaseUrl + '/' + currLocale));

            //set url when change page locale
            $('#page-locale').on('change',function(){
                var currentLang = $(this).val();
                urlPreview = $(changeSelector).attr('href');
                var res = urlPreview.replace(feBaseUrl, feBaseUrl + '/' + currentLang);
                //set href
                $(changeSelector).attr('href', spanCurUrl.replace(feBaseUrl, feBaseUrl + '/' + currentLang) + $('#page_slug').val());

                $('#span-url').html(spanCurUrl.replace(feBaseUrl, feBaseUrl + '/' + currentLang));
            });

            $('#page_title').on('keyup', function () {
                var pagetitle = $('#page_title').val();
                $('#page_slug').val($.slugify(pagetitle));
            });

            $('#page_slug').on('keyup',function(){
                $('#page_slug').val($.slugify($(this).val()));
            });
            function readURL(input) {

                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function (e) {
                        // console.log(e.target.result);
                        // $("#page_image").append('<img class="height-150 img-thumbnail" src="'+e.target.result+'">')
                        $('#page_image_preview').attr('src', e.target.result);
                        $('#page_image_preview').show();
                    }

                    reader.readAsDataURL(input.files[0]);
                }
            }

            $("#page_image").change(function () {
                readURL(this);
            });

            var checkedFeature = $('.feature_type').val();

            if (checkedFeature == 'img') {
                $('#feature-vdo-container').hide();
                $('#feature-img-container').show();
            } else {
                $('#feature-img-container').hide();
                $('#feature-vdo-container').show();
            }

            $('.feature_type').on('change', function () {
                var checkedValue = $(this).val();

                if (checkedValue == 'img') {
                    $('#feature-vdo-container').hide();
                    $('#feature-img-container').show();
                } else {
                    $('#feature-img-container').hide();
                    $('#feature-vdo-container').show();
                }
            });

            $('#category-name').on('keyup', function () {
                var cat_name = $('#category-name').val();
                $('#seo_title').val(cat_name);
                $('#category_slug').val($.slugify(cat_name));
            });

            $('#category-description').on('keyup', function () {
                $('#seo_description').val($('#category-description').val());
            });

            $('#parentCategory').select2({
                placeholder: window.SELECT_PARENT_CATEGORY,
                closeOnSelect: true,
                allowClear: true,
            });

            $('#categoryList').select2({
                placeholder: window.SELECT_CATEGORY,
                closeOnSelect: true,
                allowClear: true,
            });

            $('#newCategory').on('click', function (e) {
                e.preventDefault();
                $('.form-card').removeClass('col-md-12');
                $('.form-card').addClass('col-md-8');
                $('#category-card').toggle();
            });

            //checkbox require  validation
            function checkboxRequired() {
                var requiredCheckboxes = $('.options :checkbox[required]');

                if (requiredCheckboxes.is(':checked')) {
                    requiredCheckboxes.removeAttr('required');
                } else {
                    requiredCheckboxes.attr('required', 'required');
                }

                requiredCheckboxes.change(function () {
                    if (requiredCheckboxes.is(':checked')) {
                        requiredCheckboxes.removeAttr('required');
                    } else {
                        requiredCheckboxes.attr('required', 'required');
                    }
                });
            };

            checkboxRequired();

            //new category form submission and append added category to page form
            var categoryForm = $('#category-form');

            categoryForm.on('submit', function (e) {
                e.preventDefault();

                $.ajax({
                    data: categoryForm.serialize(),
                    dataType: 'json',
                    method: 'post',
                    url: "{{ route("admin.createNewPageCategory") }}",
                }).done(function (response) {
                    toastr.success('New category added', 'Success');
                    var data = response.data;

                    $('#categoryList').last().append(
                        `<option selected value="` + data.id + `">` + ((data.parent) ? data.parent + ' - ' : '') + data.category_name + `</option>`
                    );

                    $('.options :checkbox[required]').removeAttr('required');
                    categoryForm[0].reset();
                    $('#parentCategory').val('').trigger('change');
                }).fail(function (error) {
                    toastr.error('Error Occured', 'Error');
                });
            });


            //select 2 tag initiator
            $('#tagSelector').select2({
                placeholder: window.SELECT_TAGS,
                tags: true
            });

            //select2 initiator
            $('#categorySelector').select2({
                placeholder: window.SELECT_CATEGORY,
                minimumResultsForSearch: Infinity,
            });

            //new page form submission
            var postUrl = $('#show-preview').attr('href')+"/";

            var saveFormBtn = $('#saveForm');
            var saveBtnOriginal = saveFormBtn.html();

            $('#saveForm').on('click', function (e) {
                e.preventDefault();
                saveFormBtn.html('<i class="fa fa-spinner"></i> Processing');

                //new article form submission
                var postForm = $('#pageForm')[0];

                // Create an FormData object
                var data = new FormData(postForm);
                $.ajax({
                    data: data,
                    dataType: 'json',
                    method: 'post',
                    processData: false,  // Important!
                    contentType: false,
                    url: "{{ route("admin.createNewPage") }}",
                }).done(function (response) {
                    saveFormBtn.html(saveBtnOriginal);
                    $('#source_id').val(response.data.id);

                    $('#show-preview').attr('href', $('#span-url').html()+$('#page_slug').val());
                    $('#show-preview').show();

                    toastr.success('New page Saved', 'Success', {
                        timeOut: 5000,
                        fadeOut: 1000,
                        progressBar: true,
                    });

                }).fail(function (error) {
                    saveFormBtn.html(saveBtnOriginal);
                    var messages = error.responseJSON.msg;

                    for (message in messages) {
                        toastr.error(messages[message], 'Error',{ timeOut: 5000  });
                    }
                });
            });

            //form reset button functionality
            $("button[type='reset']").on("click", function (event) {
                event.preventDefault();
                pageForm[0].reset();
                $('#pageBody').summernote("reset");
                $('#tagSelector').val('').trigger('change');
            });
        });
    </script>
@endsection
