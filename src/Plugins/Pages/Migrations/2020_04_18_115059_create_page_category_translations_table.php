<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePageCategoryTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // create the translations table
        if (! Schema::hasTable('page_category_translations')) {
            Schema::create('page_category_translations', function (Blueprint $table) {
                $table->increments('id');
                $table->unsignedInteger('page_category_id');
                $table->string('locale', 10)->index();
                $table->string('category_name', 191)->nullable()->default(null);
                $table->string('category_slug', 191)->nullable()->default(null);
                $table->longText('category_description')->nullable()->default(null);
                $table->string('seo_title', 191)->nullable()->default(null);
                $table->text('seo_url')->nullable()->default(null);
                $table->longText('seo_description')->nullable()->default(null);
                $table->timestamps();

                $table->unique(['page_category_id', 'locale']);
                $table->foreign('page_category_id')->references('id')->on('page_category')->onDelete('cascade');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('page_category_translations');
    }
}
