@extends('Admin.layout')

@section("styles")
@endsection

@section('content')
    @include('Admin.partials.breadcumbs',['header'=>__('general.Blog'),'params'=>$article])

    <div class="content-body">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="pull-right">

                            <a class="btn btn-primary"
                               href="{{route('editArticle',['articleId'=>$article->id])}}">Edit article</a>
                        </div>


                        <h4 class="card-title" id="basic-layout-form-center">
                            {{ $article->post_title }}
                        </h4>
                        <h6>by {{ $article->author->name }}</h6>
                        @if(!$article->post_visibility)
                            <span class="badge badge-warning">Hidden</span>
                        @else
                            <span class="badge badge-success">Visible</span>
                        @endif
                    </div>
                    <div class="card-content collapse show">
                        <div class="card-body">
                            <p>
                                <b>{{implode(',',$article->categories->pluck('category_name')->all())}}</b>
                            </p>
                            <p>
                                @foreach($article->tags as $tags)
                                    <span class="badge badge-primary"> {{$tags->tag_name}}</span>
                                @endforeach
                            </p>
                            <p>
                                {{$article->post_meta_description}}
                            </p>
                            <hr>

                            <div class="articleBody">
                                {!! $article->post_body !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection
@section("scripts")
    <script src="{{ asset("vendors/js/extensions/sweetalert.min.js") }}" type="text/javascript"></script>
    <script src="{{ asset("vendors/js/extensions/dragula.min.js") }}" type="text/javascript"></script>
    <script src="{{ asset("vendors/js/extensions/toastr.min.js") }}" type="text/javascript"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6/js/select2.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote-bs4.js"></script>

    <script>
        $(document).ready(function () {
            $('#postBody').summernote({
                height: 300,
                callbacks: {
                    onImageUpload: function (files) {

                        if (!files.length) return;
                        var file = files[0];
                        // create FileReader
                        var reader = new FileReader();
                        reader.onloadend = function () {
                            // when loaded file, img's src set datauri
                            console.log("img", $("<img>"));
                            var img = $("<img>").attr({src: reader.result, width: "100%"}); // << Add here img attributes !
                            console.log("var img", img);
                            $('#postBody').summernote("insertNode", img[0]);
                        }

                        if (file) {
                            // convert fileObject to datauri
                            reader.readAsDataURL(file);
                        }


                    }
                }
                // placeholder: "Post Body",
            });

            //checkbox require  validation
            function checkboxRequired() {
                var requiredCheckboxes = $('.options :checkbox[required]');

                if (requiredCheckboxes.is(':checked')) {
                    requiredCheckboxes.removeAttr('required');
                } else {
                    requiredCheckboxes.attr('required', 'required');
                }

                requiredCheckboxes.change(function () {
                    if (requiredCheckboxes.is(':checked')) {
                        requiredCheckboxes.removeAttr('required');
                    } else {
                        requiredCheckboxes.attr('required', 'required');
                    }
                });
            };

            checkboxRequired();

            //new category form submission and append added category to article form
            var categoryForm = $('#category-form');

            categoryForm.on('submit', function (e) {
                e.preventDefault();

                $.ajax({
                    data: categoryForm.serialize(),
                    dataType: 'json',
                    method: 'post',
                    url: "{{ route("createNewCategory") }}",
                }).done(function (response) {
                    toastr.success('New category added', 'Success');
                    var data = response.data;

                    $('#categoryList').last().append(
                        `<div class="col-md-4 category" >
                            <fieldset>
                                <div class="custom-control custom-checkbox">
                                    <input required type="checkbox" class="custom-control-input"
                                           name="categories[]"
                                           id="` + data.category_name + `"
                                           value="` + data.id + `" checked>
                                    <label class="custom-control-label"
                                           for="` + data.category_name + `">` + data.category_name + `</label>
                                </div>
                            </fieldset>
                        </div>`
                    );

                    $('.options :checkbox[required]').removeAttr('required');
                    categoryForm[0].reset();

                }).fail(function (error) {
                    toastr.error('Error Occured', 'Error');
                });
            });


            //select 2 tag initiator
            $('#tagSelector').select2({
                placeholder: window.SELECT_TAGS,
                tags: true
            });

            //select2 initiator
            $('#categorySelector').select2({
                placeholder: window.SELECT_CATEGORY,
                minimumResultsForSearch: Infinity
            });


            //new article form submission
            var postForm = $('#postForm');

            postForm.on('submit', function (e) {
                e.preventDefault();
                $.ajax({
                    data: postForm.serialize(),
                    dataType: 'json',
                    method: 'post',
                    url: "{{ route("createNewArticle") }}",
                }).done(function (response) {
                    toastr.success('New article added', 'Success');
                    postForm[0].reset();
                    $('#postBody').summernote("reset");
                    $('#tagSelector').val('').trigger('change');
                }).fail(function (error) {

                    console.log(error);
                    toastr.error(error.msg, 'Error');
                });
            });

            //form reset button functionality
            $("button[type='reset']").on("click", function (event) {
                event.preventDefault();
                postForm[0].reset();
                $('#postBody').summernote("reset");
                $('#tagSelector').val('').trigger('change');
            });
        });
    </script>
@endsection