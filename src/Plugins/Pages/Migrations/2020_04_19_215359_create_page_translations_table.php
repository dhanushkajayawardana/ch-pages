<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePageTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // create the translations table
        if (! Schema::hasTable('page_translations')) {
            Schema::create('page_translations', function (Blueprint $table) {
                $table->increments('id');
                $table->unsignedInteger('page_id');
                $table->string('locale', 10)->index();
                $table->string('page_title', 191);
                $table->string('page_slug', 191);
                $table->string('page_feature_media_type', 191)->nullable()->default('img');
                $table->text('page_image')->nullable()->default(null);
                $table->text('page_video')->nullable()->default(null);
                $table->string('page_meta_title', 191)->nullable()->default(null);
                $table->text('page_meta_description')->nullable()->default(null);
                $table->unsignedInteger('page_visibility')->nullable()->default(2);
                $table->longText('page_body');
                $table->unsignedInteger('page_author');
                $table->string('page_read_time', 191)->nullable()->default(null);
                $table->dateTime('page_date')->nullable()->default(null);
                $table->timestamps();

                $table->unique(['page_id', 'locale']);
                $table->foreign('page_id')->references('id')->on('pages')->onDelete('cascade');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('page_translations');
    }
}
